package com.natruja.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.navigation.findNavController
import com.natruja.myapplication.databinding.FragmentHomeBinding
import com.natruja.myapplication.databinding.FragmentPlusBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PlusFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PlusFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var _binding: FragmentPlusBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentPlusBinding.inflate(inflater,container,false)
        return binding?.root
    }
    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.imageEqual?.setOnClickListener{
            val Num1: Long = view.findViewById<EditText>(R.id.Num1).text.toString().toLong()
            val Num2: Long = view.findViewById<EditText>(R.id.Num2).text.toString().toLong()
            val Answer = Num1.toLong() + Num2.toLong()
            val action = PlusFragmentDirections.actionPlusFragmentToAnswerFragment(Answer)
            view.findNavController().navigate(action)
        }
    }
}